### Compilation in Docker
```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://gitlab.gnome.org/GNOME/gimp.git
cd gimp

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt install -y git
apt build-dep -y gimp

git clone https://gitlab.gnome.org/GNOME/gegl.git
cd gegl
apt build-dep -y gegl
apt install -y meson ninja-build
meson build/
ninja -C build/
ninja -C build/ install
cd ../
rm -rf gegl

apt install -y libmypaint-dev gegl gjs luajit libarchive-dev mypaint-brushes libappstream-glib-dev libgtk-3-dev
meson build/

ninja -C build/

```

### QTCreator Includes
```
/usr/include/babl-0.1/
/usr/include/gegl-0.4/
/usr/include/gtk-2.0/
/usr/include/glib-2.0/
/usr/include/cairo/
/usr/include/pango-1.0/
/usr/include/harfbuzz/
/usr/lib/x86_64-linux-gnu/glib-2.0/include/
/usr/include/exiv2/
```
